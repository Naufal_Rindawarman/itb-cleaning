﻿using UnityEngine;

public class RadioBreak : Damageable {

    public float minimumForce = 4f;

    private Radio radio;

	// Use this for initialization
	void Start () {
        radio = GetComponent<Radio>();
	}

    void OnCollisionEnter(Collision coll)
    {
        //Debug.Log(coll.relativeVelocity.magnitude);
        if (coll.relativeVelocity.magnitude >= minimumForce)
        {
            SoundEffectManager.instance.TryPlaySoundAt("HitMetal", transform.position);
            radio.BreakRadio();
            this.enabled = false;
        }
    }
}
