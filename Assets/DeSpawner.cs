﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeSpawner : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        ToBeCleaned tb = other.gameObject.GetComponent<ToBeCleaned>();
        if (tb != null)
        {
            tb.ForceRemove();
        }
    }
}
