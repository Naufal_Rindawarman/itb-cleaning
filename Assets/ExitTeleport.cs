﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ExitTeleport : MonoBehaviour {

    private BoxCollider boxCollider;

    void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        if (boxCollider == null) { 
            boxCollider = gameObject.AddComponent<BoxCollider>();
        }
        boxCollider.isTrigger = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "MainCamera"){
            Debug.Log("Quit game");
            Application.Quit();
        }
    }

    void OnDrawGizmos()
    {
        boxCollider = GetComponent<BoxCollider>();
        if (boxCollider != null) Gizmos.DrawWireCube(transform.position, boxCollider.size);
    }
}
