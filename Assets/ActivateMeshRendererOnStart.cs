﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMeshRendererOnStart : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        GetComponent<MeshRenderer>().enabled = true;
	}
	
}
