﻿
using UnityEngine;

public class SpriteRandomizer : MonoBehaviour {

	public Sprite[] sprites;
	//public bool totalRandomColor = false;
	public Color[] colors;
	private SpriteRenderer sr;

	void Awake(){
		sr = GetComponent<SpriteRenderer>();
		if (sr == null) {
			Destroy (this);
		}
	}

	void Start () {
		if (sprites.Length > 0){
			sr.sprite = sprites [Random.Range (0, sprites.Length)];
		}

		if (colors.Length > 0){
			sr.color = colors [Random.Range (0, colors.Length)];
		}

	}
}
