﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialRandomizer : MonoBehaviour {

	private MeshRenderer meshRenderer;
	public int materialIndex;
	public Material[] materials;

	// Use this for initialization
	void Awake(){
		meshRenderer = GetComponent<MeshRenderer> ();
	}

	void Start () {

		if (meshRenderer == null || materials.Length == 0) {
			this.enabled = false;
			return;
		}
		int rand = Random.Range (0, materials.Length);
		//Color color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
		Material[] mats = meshRenderer.materials;
		mats[materialIndex] = materials[rand];
		meshRenderer.materials = mats;
	}

}
