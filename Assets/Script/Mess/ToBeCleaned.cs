﻿
using UnityEngine;

public abstract class ToBeCleaned : MonoBehaviour {

	protected bool cleaned = false;

	protected void Start () {
		Init ();
	}

	protected virtual void Init(){
		LevelManager.instance.AddDirtyObject ();
	}

    public void ForceRemove()
    {
        if (cleaned)
        {
            LevelManager.instance.RemoveCleanedObject();
        }
        LevelManager.instance.RemoveDirtyObject();
        Destroy(gameObject);
    }

	public void Cleans(){
		if (!cleaned) {
			cleaned = true;
			LevelManager.instance.IncreaseCleanliness (1);
		}
	}

	public void Dirties(){
		if (cleaned) {
			cleaned = false;
			LevelManager.instance.DecreaseCleanliness (1);
		}
	}



}
