﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider))]
public class Erasable : ToBeCleaned {

	public string eraseByTag;

	public int removability = 30;
	public string[] stickToTags;

	private int strength = 100;

	private SpriteRenderer spriteRend;

	protected void Start(){
		if (eraseByTag == "") {
			Debug.LogWarning ("Erase by tag empty. Delete script");
			Destroy (this);
		}			
		GetComponent<BoxCollider> ().isTrigger = true;
		spriteRend = GetComponent<SpriteRenderer> ();
		Init ();
	}

	protected override void Init(){		
		StickToSurface ();
		base.Init ();
	}

	void StickToSurface(){
		RaycastHit hit;
		Debug.Log ("Try stick");
		if (Physics.Raycast(transform.position, -transform.forward, out hit)){
			if (ArrayUtility.IsInArray<string>(hit.transform.tag, stickToTags) || stickToTags.Length == 0){
				transform.position = hit.point;
				transform.LookAt(hit.normal-hit.point);
				transform.Rotate (Vector3.right * -90);
				transform.Translate (Vector3.forward * 0.01f);
			}
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == eraseByTag) {
			tryErase();
		}
	}

	public void tryErase(){
		Debug.Log ("Try erase");
		strength -= removability;
		Color col = spriteRend.color;
		col.a = (float)strength / 100;
		spriteRend.color = col;
		if (strength <= 0) {
			Cleans ();
			this.enabled = false;
		}
	}
}
