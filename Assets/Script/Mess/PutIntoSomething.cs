﻿
using UnityEngine;

public class PutIntoSomething : ToBeCleaned {

	public string targetTag;

	protected void Start(){
		if (targetTag == "") {
			Debug.LogWarning ("Target tag empty. Delete script");
			Destroy (this);
		}
		Init ();			
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == targetTag) {
			Cleans ();
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == targetTag) {
			Dirties ();
		}
	}

	public static void AddPutIntoSomething(GameObject go, string targetTag){
		go.AddComponent<PutIntoSomething>().targetTag = targetTag;

	}
}
