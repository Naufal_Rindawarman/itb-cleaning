﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignObject : ToBeCleaned {

	private bool onFloor = false;

	protected void Start(){
		Init ();
		InvokeRepeating ("CheckIsCleaned", 0f, 1f);
	}

	void CheckIsCleaned (){
		float angle = Vector3.Angle (transform.up, Vector3.up);
		if (angle < 10 && onFloor) {
			Cleans ();
		} else {
			Dirties ();
		}
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "Floor") {
			onFloor = true;
		}
	}

	void OnCollisionExit(Collision other){
		if (other.gameObject.tag == "Floor") {
			onFloor = false;
		}
	}
}
