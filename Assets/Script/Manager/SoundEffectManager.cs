﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundEffectManager : MonoBehaviour {

	private static SoundEffectManager _instance;

	public float initialDelay = 3f;
	private bool canPlaySound = false;

	public SoundEffectPooler[] soundEffectPoolers;


	private Dictionary<string, SoundEffectPooler> soundEffectPoolersDictionary;

	public static SoundEffectManager instance {
		get {
			if (_instance == null) {
				_instance = new GameObject ("SoundEffectManager").AddComponent<SoundEffectManager> ();
			} 
			return _instance;
		}
	}

	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else if (_instance != this) {
			Destroy (this);
		}
	}

	void Start(){
		Invoke ("EnablePlayingSound", 3f);
		soundEffectPoolersDictionary = new Dictionary<string, SoundEffectPooler> ();
		for (int i = 0; i<soundEffectPoolers.Length; i++) {
			soundEffectPoolers [i].Initialize (new GameObject (soundEffectPoolers [i].name + " Sound Effect Pool"));
			soundEffectPoolersDictionary.Add (soundEffectPoolers [i].name, soundEffectPoolers [i]);
		}
	}

	void EnablePlayingSound(){
		canPlaySound = true;
	}

	public void TryPlaySoundAt(string name, Vector3 position){
		if (!canPlaySound)
			return;
		SoundEffectPooler pooler;
		if (soundEffectPoolersDictionary.TryGetValue (name, out pooler)) {
			pooler.PlaySoundEffect (position);
		}
	}
}

/*
[System.Serializable]
public class SoundEffectItem{
	public string name;
	public AudioClip clip;
	public SoundEffectPooler pooler;
}
*/

[System.Serializable]
public class SoundEffectPooler {

	public string name;
	public AudioClip[] clips;
	public int count = 5;

	private AudioSource[] audioSources;
	private int currentIndex = 0;

	// Use this for initialization
	public void Initialize (GameObject container) {
		audioSources = new AudioSource[count];
		for (int i = 0; i < count; i++) {
			GameObject obj = new GameObject(name + " AudioSource");
			AudioSource source = obj.AddComponent<AudioSource> ();
			//source.clip = clip;
			source.playOnAwake = false;
			source.spatialBlend = 1f;
			audioSources [i] = source;
		}
	}

	public void PlaySoundEffect(){
		PlaySoundEffect (Vector3.zero);
	}

	public void PlaySoundEffect(Vector3 position){
		if (currentIndex >= count) {
			currentIndex = 0;
		}

		audioSources [currentIndex].clip = clips [Random.Range (0, clips.Length)];
		audioSources [currentIndex].transform.position = position;
		audioSources [currentIndex].Play ();
	}
}
