﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class TutorialArea : MonoBehaviour {

    public GameObject tutorialSign;
    public bool onlyOnce = true;
    public bool initialActive = true;
    //public float radius = 2f;

    private CapsuleCollider capsuleCollider;

    // Use this for initialization
    void Awake()
    {
        if (tutorialSign == null)
        {
            Destroy(this);
        }
        capsuleCollider = GetComponent<CapsuleCollider>();
        if (capsuleCollider == null)
        {
            capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
        }
        //capsuleCollider.radius = radius;
        capsuleCollider.isTrigger = true;
    }

    void Start () {
        tutorialSign.SetActive(initialActive);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            tutorialSign.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            tutorialSign.SetActive(false);
            if (onlyOnce) Destroy(this);
        }
    }

    void OnDrawGizmos()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
        if (capsuleCollider != null) Gizmos.DrawWireSphere(transform.position, capsuleCollider.radius);
    }
}
