﻿
using UnityEngine;

public class SoundEffectOnCollision : MonoBehaviour {

	public string soundName;
	public float interval = 0.5f;
	private bool canPlay = true;

	void OnCollisionEnter(Collision other){
		TryPlaySound(soundName);
	}

	public void TryPlaySound(string name){
		if (canPlay) {
			SoundEffectManager.instance.TryPlaySoundAt(name, transform.position);
			canPlay = false;
			Invoke ("EnableCanPlay", interval);
		}
	}

	void EnableCanPlay(){
		canPlay = true;
	}
}
