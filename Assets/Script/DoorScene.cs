﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScene : MonoBehaviour {

    public Axis hingeAxis = Axis.Y;
	public float openThreshold = 45f;
    public int levelIndex = 0;

	private float initialAngle;

	// Use this for initialization
	void Start () {
        if (hingeAxis == Axis.X)
        {
            initialAngle = transform.localRotation.eulerAngles.x;
        }
        else if (hingeAxis == Axis.Y)
        {
            initialAngle = transform.localRotation.eulerAngles.y;
        } else
        {
            initialAngle = transform.localRotation.eulerAngles.z;
        }
		InvokeRepeating ("CheckDoorOpened", 0f, 1f);
	}
	
	// Update is called once per frame
	void CheckDoorOpened () {
        float currentAngle;
        if (hingeAxis == Axis.X)
        {
            currentAngle = transform.localRotation.eulerAngles.x;
        }
        else if (hingeAxis == Axis.Y)
        {
            currentAngle = transform.localRotation.eulerAngles.y;
        }
        else
        {
            currentAngle = transform.localRotation.eulerAngles.z;
        }
        float difference = Mathf.Abs(Mathf.DeltaAngle (currentAngle, initialAngle));
		//Debug.Log ("Current = " + currentAngle + " ,Initial = " + initialAngle + " ,Difference = " + difference);
		if (difference >= openThreshold) {
			CancelInvoke ("CheckDoorOpened");
			DoorOpened ();
			//this.enabled = false;
		}
	}

	void DoorOpened(){
        //Debug.Log ("Door opened");
        LevelManager.instance.LoadLevel(levelIndex); ;
	}
}

public enum Axis {X, Y, Z};
