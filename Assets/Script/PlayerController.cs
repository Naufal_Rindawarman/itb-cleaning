﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed =3f;
	public float rotateSpeed =30f;
	public int power = 250;

	public Transform elevateTransform;
	public Transform rotateTransform;

	public Rigidbody grabbedObjectRB;

	private Vector3 lastGrabbedObjectPosition;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.LeftAlt)){
			if (grabbedObjectRB) {
				grabbedObjectRB.transform.Rotate(Input.GetAxis ("Mouse X") * -Vector3.up * rotateSpeed * Time.deltaTime, Space.World);
				grabbedObjectRB.transform.Rotate(Input.GetAxis ("Mouse Y") * Vector3.right * rotateSpeed * Time.deltaTime, Space.World);
			}
		} else {
			transform.Translate (Input.GetAxis ("Vertical")* Vector3.forward * moveSpeed * Time.deltaTime);
			transform.Translate (Input.GetAxis ("Horizontal")* Vector3.right * moveSpeed * Time.deltaTime);

			transform.Rotate(Input.GetAxis ("Mouse X") * Vector3.up * rotateSpeed * Time.deltaTime);
			elevateTransform.Rotate(Input.GetAxis ("Mouse Y") * -Vector3.right * rotateSpeed * Time.deltaTime);

			if (Input.GetKeyDown(KeyCode.Z)){
				if (grabbedObjectRB == null){
					Grab();
				} else {
					UnGrab ();
				}
			}

			if (Input.GetKeyDown (KeyCode.F)) {
				if (grabbedObjectRB) {
					Launch ();
				}
			}
		}
		
	}

	void UpdateVelocity(){
		if (grabbedObjectRB) {
			lastGrabbedObjectPosition = grabbedObjectRB.transform.position;
		}
	}

	void FixedUpdate(){
		if (grabbedObjectRB) {
			grabbedObjectRB.transform.position = (elevateTransform.position + elevateTransform.forward);
		}
	}

	void Grab(){
		RaycastHit hit;

		if (Physics.Raycast (elevateTransform.position, elevateTransform.forward, out hit, 3f)) {
			if (hit.collider.gameObject.tag != "Floor") {
				grabbedObjectRB = hit.collider.transform.GetComponent<Rigidbody> ();
				grabbedObjectRB.constraints = RigidbodyConstraints.FreezePosition;
				grabbedObjectRB.transform.position = elevateTransform.position + elevateTransform.forward;
				lastGrabbedObjectPosition = grabbedObjectRB.transform.position;
				InvokeRepeating ("UpdateVelocity", 0f, 0.3f);
			}
		}
	}

	void UnGrab(){
		grabbedObjectRB.constraints = RigidbodyConstraints.None;
		Vector3 direction = grabbedObjectRB.transform.position - lastGrabbedObjectPosition;
		Debug.Log (direction);
		grabbedObjectRB.AddForce(direction*power); 
		CancelInvoke ("UpdateVelocity");
		grabbedObjectRB = null;
	}

	void Launch(){
		grabbedObjectRB.constraints = RigidbodyConstraints.None;
		grabbedObjectRB.AddForce(elevateTransform.forward*power); 
		CancelInvoke ("UpdateVelocity");
		grabbedObjectRB = null;
	}
}
