﻿using System.Collections;
using System.Collections.Generic;

public abstract class ArrayUtility {

	public static bool IsInArray<T>(T obj, T[] array){
		foreach (T o in array) {
			if (o.Equals (obj)) {
				return true;
			}
		}
		return false;
	}


}
