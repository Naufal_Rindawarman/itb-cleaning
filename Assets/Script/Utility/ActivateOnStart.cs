﻿
using UnityEngine;

public class ActivateOnStart : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        gameObject.SetActive(true);
	}

}
