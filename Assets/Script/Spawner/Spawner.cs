﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public ToBeCleaned[] dirtyObjects;
	public int numberOfObjects = 5;
	public bool randomDirection = true;

	// Use this for initialization
	void Awake(){
		if (dirtyObjects.Length == 0) {
			Destroy (this);
		}
		/*
		boxColl = GetComponent<BoxCollider> ();
		if (boxColl == null) {
			boxColl = gameObject.AddComponent<BoxCollider> ();
		}
		*/
	}

	void Start () {
		StartSpawn ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void StartSpawn(){
		int n = 0;
		Quaternion orient;
		while (n < numberOfObjects) {
			int r = Random.Range (0, dirtyObjects.Length);
			//Vector3 pos = transform.position + new Vector3 (Random.Range (-boxColl.size.x / 2, boxColl.size.x / 2), Random.Range (-boxColl.size.y / 2, boxColl.size.y / 2), Random.Range (-boxColl.size.z / 2, boxColl.size.z / 2));
			Vector3 pos = transform.position + new Vector3 (Random.Range (-transform.localScale.x / 2, transform.localScale.x / 2), transform.localScale.y / 2, Random.Range (-transform.localScale.z / 2, transform.localScale.z / 2));
			if (randomDirection) {
				orient = Quaternion.LookRotation (new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, -0.1f), Random.Range (-1f, 1f)) - pos);
			} else {
				orient = dirtyObjects [r].transform.rotation;
			}
			Instantiate (dirtyObjects [r], pos, orient);
			n++;
		}
	}

	void OnDrawGizmos(){
		Gizmos.DrawWireCube (transform.position, transform.localScale);
	}
}
