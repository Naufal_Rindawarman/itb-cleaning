﻿
using UnityEngine;
using VRTK;
using VRTK.SecondaryControllerGrabActions;
using VRTK.GrabAttachMechanics;

public class Fragile : Damageable {

	public int minimumForce = 4;
	public string breakSoundName;

	// Use this for initialization
	void OnCollisionEnter(Collision coll){
		Debug.Log (coll.relativeVelocity.magnitude);
		if (coll.relativeVelocity.magnitude >= minimumForce){
			BreakApart ();
		}
	}

	void BreakApart(){
		foreach (Transform face in GetComponentsInChildren<Transform>()){
			if (face.transform.name == transform.name) continue;
			if (face.parent != null) {
				if (face.parent.name != transform.name) continue;
			}
			ExplodeFace(face, 0f);
		}
		//GetComponent<VRTK_InteractGrab> ().ForceRelease ();
		if (breakSoundName != "") {
			SoundEffectManager.instance.TryPlaySoundAt (breakSoundName, transform.position);
		}
        LevelManager.instance.AddPenalty(penalty);
		Destroy (gameObject, 2f);
		
	}

	private void ExplodeFace(Transform face, float force)
	{
		face.transform.SetParent(null);
		Rigidbody rb = face.gameObject.AddComponent<Rigidbody>();
		face.gameObject.AddComponent<MeshCollider> ().convex = true;
		rb.isKinematic = false;
		rb.useGravity = true;
		PutIntoSomething.AddPutIntoSomething (face.gameObject, "TrashCan");
		face.gameObject.AddComponent<VRTK_InteractableObject> ().isGrabbable = true;
		face.gameObject.AddComponent<VRTK_SwapControllerGrabAction> ();
		face.gameObject.AddComponent<VRTK_ChildOfControllerGrabAttach> ();
		//rb.AddExplosionForce(force, Vector3.zero, 0f);
		//Destroy(face.gameObject, 10f);
	}
}
