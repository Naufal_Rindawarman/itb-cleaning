﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Radio : MonoBehaviour {

    public AudioClip[] clips;
    public bool shuffle = false;

    private int currentTrack = 0;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        source.spatialBlend = 1f;
        PlayRandom();
	}
	
	// Update is called once per frame
	void PlayRandom()
    {
        if (source.isPlaying)
        {
            return;
        }
        currentTrack = Random.RandomRange(0, clips.Length);
        PlayTrack(currentTrack);        
    }

    void PlayTrack(int i)
    {
        source.clip = clips[i];
        source.Play();
        float time = clips.Length;
        if (shuffle)
        {
            Invoke("PlayShuffle", time);
        } else
        {
            Invoke("PlayNext", time);
        }
        
    }

    void PlayNext()
    {
        currentTrack++;
        if (currentTrack >= clips.Length)
        {
            currentTrack = 0;
        }
    }

    void PlayShuffle()
    {
        int r;
        do
        {
            r = Random.RandomRange(0, clips.Length);
        } while (r == currentTrack);
        currentTrack = r;
        PlayTrack(currentTrack);
    }

    public void BreakRadio()
    {
        source.Stop();
        CancelInvoke();
        this.enabled = false;
    }
}
